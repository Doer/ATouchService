# ATouch

## **ATouch线上文档请点击下面链接** 

[ATouch线上文档](http://guanglundz.com/atouch)  

## ATouch安卓端后台源码

![效果图](https://images.gitee.com/uploads/images/2020/0408/110002_b982beff_683968.png "atouch2.png")
  
* 介绍博客
[https://www.cnblogs.com/guanglun/p/10927196.html](https://www.cnblogs.com/guanglun/p/10927196.html)  

* ATouch安卓APP源码
[https://gitee.com/guanglunking/ATouch](https://gitee.com/guanglunking/ATouch)  
【开发环境：AndroidStudio】

* ATouch板子源码
[https://gitee.com/guanglunking/ESP32_CH374U](https://gitee.com/guanglunking/ESP32_CH374U)  
【开发环境：Linux SDK:ESP-DIF3.2】

* ATouch安卓后台程序源码
[https://gitee.com/guanglunking/ATouchService](https://gitee.com/guanglunking/ATouchService)   
【开发环境：android-ndk-r13b】

* APP下载地址：   
[https://gitee.com/guanglunking/ATouch/releases](https://gitee.com/guanglunking/ATouch/releases)

* 安卓后台可执行文件下载地址：   
[https://gitee.com/guanglunking/ATouchService/releases](https://gitee.com/guanglunking/ATouchService/releases)

* 板子固件下载地址：   
[https://gitee.com/guanglunking/ESP32_CH374U/releases](https://gitee.com/guanglunking/ESP32_CH374U/releases)

* 淘宝店铺
[https://item.taobao.com/item.htm?id=595635571591](https://item.taobao.com/item.htm?id=595635571591)  

* 演示视频
[https://www.bilibili.com/video/av53687214](https://www.bilibili.com/video/av53687214)  

![设置界面](https://images.gitee.com/uploads/images/2020/0408/110030_b23d7f55_683968.png "atouch3.png")
